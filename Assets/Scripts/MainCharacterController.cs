﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class StatsUpdateEvent : UnityEvent<int>
{

}

public class MainCharacterController : MonoBehaviour
{
    [SerializeField]
    private StatsUpdateEvent OnLifeUpdate = new StatsUpdateEvent();

    [SerializeField]
    private StatsUpdateEvent OnScoreUpdate = new StatsUpdateEvent();

	[SerializeField]
	private UnityEvent OnGameOver = new UnityEvent();

    [SerializeField]
    private float jumpStrength = 1;

    private int health = 3;

    private int score = 0;

    // Start is called before the first frame update
    void Start()
    { 
        health = 3;
        score = 0;
		OnLifeUpdate.Invoke(health);
        OnScoreUpdate.Invoke(score);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            transform.GetComponent<Rigidbody>().AddForce(0, jumpStrength, 0);
        }
    }

    private void PerformGameOver()
    {
        OnGameOver.Invoke();
        Destroy(gameObject); // TODO: This can be an explosion or something...
    }

    public void DidCollideWithBoundary(string type)
	{
        switch (type)
        {
            case "GameOver":
                PerformGameOver();
                break;
            case "Hit":
                health -= 1;
                OnLifeUpdate.Invoke(health);
                if (health == 0)
                    PerformGameOver();
                break;
            case "Passage":
                score += 1;
                OnScoreUpdate.Invoke(score);
                break;
            default:
                Debug.Log(string.Format("Bad type: {0}", type));
                Debug.DebugBreak();
                break;
        }
	}
}
