﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundaryController : MonoBehaviour
{
    [SerializeField]
    private string targetName = "";

    [SerializeField]
    private string collisionType = "";

    private bool didCollide;

    // Start is called before the first frame update
    void Start()
    {
        didCollide = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void HandleCollision(Collider other)
    {
        // Collision only triggered once per boundary
        if (!didCollide && other.gameObject.name.Contains(targetName))
        {
            didCollide = true;
            other.gameObject.GetComponent<MainCharacterController>()
                .DidCollideWithBoundary(collisionType);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        HandleCollision(collision.collider);
    }

    private void OnTriggerEnter(Collider other)
    {
        HandleCollision(other);
    }
}
