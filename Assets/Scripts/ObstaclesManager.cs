﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesManager : MonoBehaviour
{
    [SerializeField]
    private GameObject prefabObstacle;

    [SerializeField]
    private GameObject prefabPassage;

    [SerializeField]
    private Transform spawnLocation;

    [SerializeField]
    private float obstacleSpeed = 1;

    [SerializeField]
    private float spawnInterval = 1;

    [SerializeField]
    private float screenLength = 10;

    [SerializeField]
    private float screenHeight = 10;

    [SerializeField]
    private float doorwaySize = 2;

    private LinkedList<GameObject> obstacles = new LinkedList<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        Invoke("Spawn", spawnInterval);
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void InstantiateObstacle(float height, bool top)
    {
        GameObject obstacle =
            Instantiate(prefabObstacle, spawnLocation.position, Quaternion.identity, transform);

        float topModifier = top ? 1 : -1;

        obstacle.transform.localScale = new Vector3(1, height, 1);
        obstacle.transform.Translate(0, topModifier * ((screenHeight - height) / 2.0f), 0);

        var controller = obstacle.GetComponent<ObstacleController>();
        controller.Speed = obstacleSpeed;

        Destroy(obstacle, 10f); // 10 seconds seems like enough time
    }

    private void InstantiatePassage(float topHeight)
    {
        GameObject passage =
            Instantiate(prefabPassage, spawnLocation.position, Quaternion.identity, transform);

        passage.transform.localScale = new Vector3(1, doorwaySize, 1);
        passage.transform.Translate(0, (screenHeight - doorwaySize) / 2.0f - topHeight, 0);

        var controller = passage.GetComponent<ObstacleController>();
        controller.Speed = obstacleSpeed;

        Destroy(passage, 10f); // 10 seconds seems like enough time
    }

    void Spawn()
    {
        float min = 1;
        float max = screenHeight - doorwaySize - 1;

        float topHeight = Random.Range(min, max);

        InstantiateObstacle(topHeight, true);
        InstantiateObstacle(screenHeight - (topHeight + doorwaySize), false);
        InstantiatePassage(topHeight);

        // Repeat. Not using InvokeRepeating because obstacleSpeed and distance could change due to levels.
        Invoke("Spawn", spawnInterval);
    }

    public void HandleScoreEvent(int score)
    {
        if (spawnInterval >= 0.5f) // lower limit
        {
            spawnInterval -= 0.002f;
        }
    }
}
