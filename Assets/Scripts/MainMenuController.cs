﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    [SerializeField]
    private GameObject dialogAreYouSure;

    public void NewGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
    }

    public bool ShowDialogAreYouSure
    {
        set
        {
            dialogAreYouSure.SetActive(value);
        }
        get
        {
            return dialogAreYouSure.activeInHierarchy;
        }
    }

    public void QuitGame()
    {
        Debug.Log("QUIT!");
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
