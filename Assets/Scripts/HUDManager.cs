﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    [SerializeField]
    private GameObject lifeTextbox;

    [SerializeField]
    private GameObject scoreTextbox;

    [SerializeField]
    private GameObject gameOverPanel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HandleLifeEvent(int health)
    {
        lifeTextbox.GetComponent<Text>().text = string.Format("Health: {0}", health);
    }

    public void HandleScoreEvent(int score)
    {
        scoreTextbox.GetComponent<Text>().text = string.Format("Score: {0}", score);
    }

    public void HandleGameOver()
    {
        StartCoroutine(GameOverCoroutine());
    }

    private IEnumerator GameOverCoroutine()
    {
        gameOverPanel.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene("MainMenu");
    }
}
